# Test project nginx.conf 

Sample project

## Let's start

Copy nginx.conf /etc/nginx/nginx.conf


### Prerequisites

Nginx web server

### Installing

Installing nginx:

* On ubuntu: apt-get install nginx
* On RHEL/Centos: yum install nginx

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
curl https://localhost
```
### And coding style tests

Explain what these tests test and why


## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* Test
* Notepad

## Versioning

We use [gitlab](https://gitlab.rebrainme.com/e.simigin/rebrain-devops-task-checkout.git) 

## Authors
- E.simigin@expobank.ru

## License

Free for non commecrical usage

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
